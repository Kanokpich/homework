
public class SanjiAdapter implements AnimeHero {
    private Sanji sanji;
    public SanjiAdapter(Sanji sanji){
        this.sanji = sanji;
    }
    public String attack(){
        return this.sanji.kick();
    }
}
